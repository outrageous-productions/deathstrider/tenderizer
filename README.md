# Tenderizer

A weapon addon for [Deathstrider](https://gitlab.com/accensi/deathstrider).

---

A 100 round automatic shotgun that fires two rounds per shot, and has an adjustable choke.

---

See [credits.txt](./credits.txt) for attributions.
